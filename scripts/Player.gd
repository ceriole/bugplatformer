# Player script

extends Node2D

signal on_health_init(health, max_health)
signal on_energy_init(energy, max_energy)
signal on_coins_init(coins)

signal on_health_updated(health, amount)
signal on_max_health_updated(max_health)
signal on_energy_updated(energy, amount)
signal on_max_energy_updated(max_energy)
signal on_coins_updated(coins)

signal on_defeated()
signal on_energy_empty()
signal on_energy_full()
signal on_energy_charging()
signal on_energy_depleting()
signal on_hit()
signal on_grounded_updated(grounded)

signal on_move(pos, vel)

signal on_jump()
signal on_land()

signal on_laser_begin()
signal on_laser_end()



var velocity = Vector2()
var move_speed = Globals.MOVE_SPEED * Globals.UNIT_SIZE

var gravity
var max_jump_velocity
var min_jump_velocity

var max_jump_height = Globals.MAX_JUMP_HEIGHT * Globals.UNIT_SIZE
var min_jump_height = Globals.MIN_JUMP_HEIGHT * Globals.UNIT_SIZE
var jump_duration = Globals.JUMP_DURATION

var grounded = true
var jumping = false

export (int) var max_health = 20 setget _set_max_health
export (int) var max_energy = 100 setget _set_max_energy

onready var health = max_health setget _set_health
onready var energy = max_energy setget _set_energy
var coins = 0 setget _set_coins
var charge_speed = 20
var charging = false

var facing = 1

onready var phys_body = $Logic

onready var vis_body = $Body
onready var anim_player = $Body/BugRig/AnimationPlayer
onready var effects_player = $Logic/EffectsAnimation

onready var particles_land_dust = $Body/LandDust
onready var particles_foot_dust = $Body/FootDust
onready var particles_energy_star = $Body/EnergyStar

onready var invul_timer = $Logic/InvulTimer
onready var charge_timer = $Logic/RechargeTimer
onready var land_dust_timer = $Logic/LandDustTimer

onready var laser = $Logic/Laser
onready var laser_point = $Logic/LaserPoint

onready var ground_raycasts = $Logic/GroundRaycasts
onready var bounce_raycasts = $Logic/BounceRaycasts

func _ready():
	gravity = 2 * max_jump_height / pow(jump_duration, 2)
	max_jump_velocity = -sqrt(2 * gravity * max_jump_height)
	min_jump_velocity = -sqrt(2 * gravity * min_jump_height)
	
	emit_signal("on_health_init", health, max_health)
	emit_signal("on_energy_init", energy, max_energy)
	emit_signal("on_coins_init", coins)

func _process(delta):
	if charging:
		_set_energy(energy + charge_speed * delta)
	
	particles_foot_dust.emitting = grounded && abs(velocity.x) > (move_speed * 0.75)
	
	_assign_animation()
	
	if laser.is_casting:
		if laser.is_colliding():
			var target : Node2D = laser.get_collider().get_parent()
			if target.is_in_group("enemies") && target.has_method("on_laser_hit"):
				target.on_laser_hit(self, 0.25)

func _physics_process(delta):
	_get_input()
	velocity.y += gravity * delta
	
	if jumping && velocity.y >= 0:
		jumping = false
	
	_check_bounce(delta)
	velocity = phys_body.move_and_slide(velocity, Globals.UP, true, 4)
	if velocity.length() > 0:
		emit_signal("on_move", phys_body.position, velocity)
	
	var grounded_now = _check_grounded()
	if grounded != grounded_now:
		emit_signal("on_grounded_updated", grounded_now)
		if grounded_now:
			emit_signal("on_land")
			if land_dust_timer.is_stopped():
				particles_land_dust.emitting = true
				land_dust_timer.start(particles_land_dust.lifetime + 0.1)
		grounded = grounded_now

func _input(event):
	if event.is_action_pressed("jump") && grounded:
		emit_signal("on_jump")
		velocity.y = max_jump_velocity
		jumping = true
	
	if event.is_action_released("jump") && velocity.y < min_jump_velocity && jumping:
		velocity.y = min_jump_velocity

func _get_input():
	var move_dir = -int(Input.is_action_pressed("left")) + int(Input.is_action_pressed("right"))
	velocity.x = lerp(velocity.x, move_speed * move_dir, _get_h_weight())
	
	if move_dir != 0:
		facing = move_dir
		vis_body.scale.x = facing
		laser.scale.x = facing
	
	if move_dir == 0:
		if (velocity.x < 0 && velocity.x > -1) || (velocity.x > 0 && velocity.x < 1):
			velocity.x = 0
	
	if Input.is_action_pressed("shoot") && energy > 0:
		if !laser.is_casting:
			emit_signal("on_laser_begin")
		laser.position = laser_point.position * Vector2(facing, 1)
		laser.set_casting(true)
		deplete_energy(0.5)
	else:
		if laser.is_casting:
			emit_signal("on_laser_end")
		laser.set_casting(false)

func _get_h_weight():
	return 0.2 if grounded else 0.1

func _check_grounded(raycasts = ground_raycasts):
	for raycast in raycasts.get_children():
		if raycast.is_colliding():
			return true
	return false

func _check_bounce(delta):
	if velocity.y > 0:
		for raycast in bounce_raycasts.get_children():
			raycast.cast_to = Vector2.DOWN * velocity * delta + Vector2.DOWN
			raycast.force_raycast_update()
			if raycast.is_colliding() && raycast.get_collision_normal() == Vector2.UP:
				velocity.y = (raycast.get_collision_point() - raycast.global_position - Vector2.DOWN).y / delta
				raycast.get_collider().entity.call_deferred("be_bounced_upon", self)

func _assign_animation():
	var anim = "idle"
	
	if !grounded:
		if velocity.y < 0:
			anim = "jump"
		elif velocity.y > 0:
			anim = "fall"
	elif velocity.x != 0:
		anim = "walk"
	
	if anim_player.assigned_animation != anim:
		anim_player.play(anim)

func damage(amount):
	# only take damage if not in i-frames
	if invul_timer.is_stopped():
		invul_timer.start()
		_set_health(health - amount)
		effects_player.play("damage")
		effects_player.queue("flash")

func deplete_energy(amount):
	_set_energy(energy - amount)
	emit_signal("on_energy_depleting")
	charge_timer.start()

func add_energy(amount):
	_set_energy(energy + amount)
	emit_signal("on_energy_charging")

func defeat():
	_set_health(0)
	emit_signal("on_defeated")

func _set_health(value):
	var prev_health = health
	health = clamp(value, 0, max_health)
	if health != prev_health:
		emit_signal("on_health_updated", health, health - prev_health)
		if health < prev_health:
			emit_signal("on_hit")
		if health == 0:
			defeat()

func _set_max_health(max_value):
	var health_percentage = (health / max_health) if health != null else 0
	max_health = max_value
	health = max_health * health_percentage
	emit_signal("on_max_health_updated", max_energy)
	emit_signal("on_health_updated", energy, 0)

func _set_energy(value):
	var prev_energy = energy
	energy = clamp(value, 0, max_energy)
	if energy != prev_energy:
		emit_signal("on_energy_updated", energy, energy - prev_energy)
		if energy < prev_energy:
			emit_signal("on_energy_depleting")
			# cancel charging if used energy
			charging = false
		if energy == 0:
			emit_signal("on_energy_empty")
		elif energy == max_energy:
			emit_signal("on_energy_full")
			particles_energy_star.restart()
			particles_energy_star.emitting = true
			# cancel charging if energy full
			charging = false

func _set_max_energy(max_value):
	var energy_percentage = (energy / max_energy) if energy != null else 0
	max_energy = max_value
	energy = max_energy * energy_percentage
	emit_signal("on_max_energy_updated", max_energy)
	emit_signal("on_energy_updated", energy, 0)

func _set_coins(value):
	coins = value
	emit_signal("on_coins_updated", coins)

func _on_invul_timeout():
	# when invulnrability timer is done, reset animation
	effects_player.play("rest")

func _on_recharge_timeout():
	# when recharge timer is done, start charging
	emit_signal("on_energy_charging")
	charging = true

func bounce(bounce_height = Globals.BOUNCE_HEIGHT):
	velocity.y = -sqrt(2 * gravity * bounce_height * Globals.UNIT_SIZE)
