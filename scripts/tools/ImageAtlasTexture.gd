tool
extends ImageTexture
class_name ImageAtlasTexture


### NOTE: This script is far from being used as part of a plugin
### It basically gets a Texture's Image, uses Image.get_rect() on it and sets is as the image


export var atlas: Texture setget set_atlas
export var region: Rect2 setget set_region


func _init():
	update_image()


func set_atlas(val):
	atlas = val
	if atlas and not atlas.is_connected("changed", self, "update_image"):
		var __ = atlas.connect("changed", self, "update_image")
	update_image()
	emit_changed()


func set_region(val):
	region = val
	update_image()
	emit_changed()


func update_image():
	if !atlas: return
	if !region.size: return
	var p_flags = flags
	# Here is where the magic happens
	var p_image = atlas.get_data().get_rect(region)
	create_from_image(p_image)
	flags = p_flags
