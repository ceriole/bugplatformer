extends RayCast2D

var width = 5

var is_casting := false setget set_casting

func _ready() -> void:
	set_physics_process(false)
	$Line2D.points[1] = Vector2.ZERO

func _physics_process(_delta: float) -> void:
	var cast_point := cast_to
	force_raycast_update()
	
	if is_colliding():
		cast_point = to_local(get_collision_point())
		$CollisionParticles.global_rotation = get_collision_normal().angle()
		$CollisionParticles.position = cast_point
	
	$Line2D.points[1] = cast_point
	$BeamParticles.position = cast_point * 0.5
	$BeamParticles.process_material.emission_box_extents.x = cast_point.length() * 0.5
	
	$CollisionParticles.emitting = is_colliding()

func set_casting(cast: bool) -> void:
	if cast == is_casting:
		return
	is_casting = cast
	
	$BeamParticles.emitting = is_casting
	$CastingParticles.emitting = is_casting
	
	if is_casting:
		appear()
	else:
		$CollisionParticles.emitting = false
		disappear()
	
	set_physics_process(is_casting)

func appear() -> void:
	$Tween.remove_all()
	$Tween.interpolate_property($Line2D, "width", 0, width, 0.1)
	$Tween.start()

func disappear() -> void:
	$Tween.remove_all()
	$Tween.interpolate_property($Line2D, "width", width, 0, 0.05)
	$Tween.start()
