extends Node

const UNIT_SIZE = 18 * 2
const UP = Vector2(0, -1)
const MOVE_SPEED = 10
const MAX_JUMP_HEIGHT = 6
const MIN_JUMP_HEIGHT = 2
const JUMP_DURATION = 0.5
const BOUNCE_HEIGHT = 2
