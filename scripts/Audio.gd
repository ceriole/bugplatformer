extends Node

func _on_jump():
	$SoundJump.play()

func _on_big_jump():
	$SoundBigJump.play()

func _on_land():
	$SoundLand.play()

func _on_hit():
	$SoundHurt.play()

func _on_energy_charging():
	$Energy/SoundRecharge.play()

func _on_energy_full():
	if $Energy/SoundRecharge.playing:
		yield($Energy/SoundRecharge, "finished")
	$Energy/SoundFull.play()

func _on_energy_empty():
	$Energy/SoundDepleted.play()

func _on_laser_begin():
	$Energy/SoundGrain.play()

func _on_laser_end():
	$Energy/SoundGrain.stop()

func _on_enemy_fattened(stage):
	$SoundFatten.pitch_scale = 1 + stage / 4
	$SoundFatten.play()

func _on_enemy_unfattened(_stage):
	$SoundUnfatten.play()

func _on_energypickup_collected(_by):
	$Energy/SoundRecharge.play()
