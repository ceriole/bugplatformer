extends NinePatchRect

onready var value_lbl = $Value

func _on_value_updated(value):
	value_lbl.text = str(value)


func _on_init(value):
	value_lbl.text = str(value)
