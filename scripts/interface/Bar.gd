extends HBoxContainer

onready var bar_over = $Bar/BarOver
onready var bar_under = $Bar/BarUnder
onready var update_tween = $UpdateTween
onready var pulse_tween = $PulseTween
onready var flash_tween = $FlashTween

onready var counter_value = $Counter/Value

export (float, 0, 1, 0.05) var interp_length = 0.4
export (String) var counter_string = "%d/%d"

export (Color) var healthy_color = Color.green
export (Color) var caution_color = Color.yellow
export (Color) var critical_color = Color.red

export (float, 0, 1, 0.05) var caution_zone = 0.5
export (float, 0, 1, 0.05) var critical_zone = 0.25

export (Color) var flash_color = Color.orangered
export (int, 0, 10) var flash_amount = 4
export (float, 0, 1, 0.05) var flash_rate = 0.05

export (bool) var pulse_on_critical = true
export (Color) var pulse_color = Color.orange
export (float, 0, 5, 0.05) var pulse_length = 1.2

func _on_init(value, max_value):
	bar_over.max_value = max_value
	bar_under.max_value = max_value
	bar_over.value = value
	bar_under.value = value
	_update_counter(value, max_value)
	_assign_color(value)

func _on_value_updated(value, amount):
	bar_over.value = value
	update_tween.interpolate_property(bar_under, "value", bar_under.value, value, interp_length, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	update_tween.start()
	
	_assign_color(value)
	if amount < 0:
		_flash_damage()
	
	_update_counter(value, bar_over.max_value)

func _flash_damage():
	for i in range(flash_amount * 2):
		var color = bar_over.tint_progress if i % 2 == 1 else flash_color
		var time = flash_rate * i + flash_rate
		flash_tween.interpolate_callback(bar_over, time, "set", "tint_progress", color)
	flash_tween.start()

func _update_counter(value, max_value):
	counter_value.text = counter_string % [value, max_value]

func _assign_color(value):
	if value == 0:
		pulse_tween.set_active(false)
	if value <= bar_over.max_value * critical_zone:
		if pulse_on_critical:
			if !pulse_tween.is_active():
				pulse_tween.interpolate_property(bar_over, "tint_progress", pulse_color, critical_color, pulse_length, Tween.TRANS_SINE, Tween.EASE_IN_OUT)
				pulse_tween.start()
		else:
			bar_over.tint_progress = critical_color
	else:
		pulse_tween.set_active(false)
		if value <= bar_over.max_value * caution_zone:
			bar_over.tint_progress = caution_color
		else:
			bar_over.tint_progress = healthy_color

func _on_max_value_updated(max_value):
	bar_over.max_value = max_value
	bar_under.max_value = max_value
	_update_counter(bar_over.value, max_value)
