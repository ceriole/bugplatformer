extends KinematicBody2D

signal on_collected(by)

const UP = Vector2(0, -1)

var velocity = Vector2()
var move_speed = 180
var gravity = 1200

export (float, 0, 100, 1) var energy_amount = 50

func _physics_process(delta):
	velocity.y += gravity * delta
	
	velocity = move_and_slide(velocity, UP, false, 2, 0.758, false)

func _can_collect(target) -> bool:
	return target.is_in_group("player") && target.energy < target.max_energy

func collect(target):
	if _can_collect(target):
		target.add_energy(energy_amount)
		emit_signal("on_collected", target)
		queue_free()

func _on_body_entered(body):
	var parent = body.get_parent()
	collect(parent)

func _on_bounce_timeout():
	if is_on_floor():
		velocity.y = -150
