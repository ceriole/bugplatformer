extends Position2D

signal on_spawn(obj, index, remaining)
signal spawns_complete()

export (PackedScene) var spawn_scene = null

export (bool) var autostart: bool = true
export (bool) var single_spawn: bool = true
export (float, 0, 4096, 1) var spawn_delay: int = 5

export (Vector2) var spawn_offset: Vector2 = Vector2.ZERO
export (Vector2) var random_offset: Vector2 = Vector2.ZERO
export (Vector2) var spawn_velocity: Vector2 = Vector2.ZERO

export (int, 0, 10000, 1) var max_spawns: int = 0

var spawn_count: int = 0

func _ready():
	randomize()
	$SpawnTimer.wait_time = spawn_delay
	if autostart:
		$SpawnTimer.start()
	if single_spawn:
		$SpawnTimer.one_shot = true

func _spawn():
	if !_can_spawn():
		return
	var obj = spawn_scene.instance()
	var loc = position + spawn_offset
	if random_offset.length() > 0:
		loc += (-random_offset / 2) + (random_offset * Vector2(randf(), randf()))
	obj.position = loc
	
	if obj is KinematicBody2D && spawn_velocity.length() > 0:
		var kb2d : KinematicBody2D = obj
		kb2d.velocity = spawn_velocity
	
	if single_spawn:
		obj.connect("tree_exited", $SpawnTimer, "start")
	get_parent().add_child(obj)
	spawn_count += 1
	emit_signal("on_spawn", obj, spawn_count, max_spawns - spawn_count if max_spawns > 0 else -1)
	if max_spawns > 0 && spawn_count == max_spawns:
		emit_signal("spawns_complete")

func _can_spawn() -> bool:
	return spawn_count < max_spawns if max_spawns > 0 else true

func _on_spawn_timeout():
	_spawn()
