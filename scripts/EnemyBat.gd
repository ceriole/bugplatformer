extends Node2D

signal on_grounded_updated(grounded)
signal on_land()

signal on_fattened(stage)
signal on_unfattened(stage)

signal on_big_bounce()
signal on_small_bounce()

const UP = Vector2(0, -1)
const SNAP = 65

var velocity = Vector2()
var gravity = 1200
var grounded = true
var falling = false
var facing = -1

var fatness = 0
var max_fatness = 100
var losing_fatness = false

onready var phys_body = $Logic

onready var vis_body = $Body
onready var sprite = $Body/AnimatedSprite

onready var loss_timer = $Logic/LossTimer

onready var fatten_particles = $Logic/FattenParticles
onready var unfatten_particles = $Logic/UnfattenParticles

onready var bounce_shape = $Logic/BounceArea/BounceShape
onready var bounce_anim = $Logic/BounceAnimation

func _ready():
	sprite.play("default")
	bounce_anim.play("idle")

func _physics_process(delta):
	if falling:
		velocity.y += gravity * delta
	
	velocity = phys_body.move_and_slide(velocity, UP, SNAP)
	
	if facing < 0:
		vis_body.scale.x = 1
	elif facing > 0:
		vis_body.scale.x = -1
		
	_check_grounded()
	_apply_animation()

func _apply_animation():
	if fatness > 0:
		sprite.playing = false
		sprite.animation = "inflate"
		var frame_count = sprite.frames.get_frame_count(sprite.animation)
		var new_frame = round(frame_count * (fatness / max_fatness))
		if sprite.frame != new_frame  && new_frame != frame_count:
			if !losing_fatness:
				fatten_particles.restart()
				fatten_particles.emitting = true
				emit_signal("on_fattened", new_frame)
			else:
				unfatten_particles.restart()
				unfatten_particles.emitting = true
				emit_signal("on_unfattened", new_frame)
		
		sprite.frame = new_frame
	else:
		sprite.play("default")

func fatten(amount):
	_set_fatness(fatness + amount)

func _set_fatness(value):
	var prev_fatness = fatness
	fatness = clamp(value, 0, max_fatness)
	if fatness != prev_fatness:
		if fatness > prev_fatness:
			losing_fatness = false
			loss_timer.start()
		if fatness == 0:
			losing_fatness = false

func _check_grounded():
	var new_value = phys_body.is_on_floor()
	if grounded != new_value:
		emit_signal("on_grounded_updated", new_value)
		if new_value:
			emit_signal("on_land")
		grounded = new_value

func on_laser_hit(_source, amount):
	fatten(amount)

func _on_loss_timeout():
	losing_fatness = true

func _on_bug_move(pos, _vel):
	var bug_localpos = phys_body.to_local(pos)
	if bug_localpos.x < 0:
		facing = -1
	elif bug_localpos.x > 0:
		facing = 1

func be_bounced_upon(bouncer):
	if bouncer.has_method("bounce"):
		if sprite.frame == sprite.frames.get_frame_count("inflate") - 1:
			bouncer.bounce(8)
			bounce_anim.stop()
			bounce_anim.play("bounce_big")
			emit_signal("on_big_bounce")
		else:
			bouncer.bounce(2)
			bounce_anim.stop()
			bounce_anim.play("bounce_small")
			emit_signal("on_small_bounce")
