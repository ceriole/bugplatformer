class_name Enemy
extends Node2D
# An enemy that does not move, but can cause damage and be grown.


# The amount that this enemy can grow not including default.
# Determines what animations and logic it uses.
# Can also effect the level and other actors (player, enemies) differently.
export (int) var growth_stages = 3
# The current growth stage. Change this to set the enemy's visual size (stage).
export (int) var current_stage = 0

# The maximum amount of 'growth points' this enemy can take.
# max_growth / growth_stages = the amount needed to grow one stage.
export (int) var max_growth = 100

# Seconds before this enemy begins to lose size.
export (float) var shrink_time = 3
# How much growth that this enemy will lose per second.
export (int) var growth_loss = 25

enum GrowthState{IDLE, SHRINK, GROW}

# Current groth
var growth_points = 0
var growth_state = GrowthState.IDLE

onready var shrink_timer: Timer = $ShrinkTimer

func _ready():
	growth_stages = max(growth_stages, 0) # Keep growth_stages >= 0
	
	growth_points = 0

func _process(delta):
	current_stage = growth_points / growth_stages

func on_grow_hit(amt):
	growth_points = min(growth_points + amt, max_growth)
	growth_state = GrowthState.GROW 
	shrink_timer.time_left = shrink_time
