extends TileMap

export(float) var y_speed = 5
export(float) var y_maxoffset = 18

var time_start = 0
var time_now = 0

var base_pos = 0
var y_offset = 0

func _ready():
	y_offset = 0
	time_start = OS.get_ticks_msec()
	base_pos = position

func _process(delta):
	time_now = OS.get_ticks_msec()
	var time_elapsed = time_now - time_start
	y_offset = y_maxoffset * sin((time_elapsed / (1000.0 * y_speed)) * 2.0 * PI)
	position.y = base_pos.y + y_offset
